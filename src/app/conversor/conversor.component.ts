import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

   datoSend = {unidat: "", uniconv: "", dat: ""};
   //datoReceived ={statusCode:'' ,uni_dat: "", uni_conv:"", dat:"", dat_conv:""};
  datoReceived: any;
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

  }


  
  Convertir(dato, unidaddato, unidadconv) {
    //Convertir() {
    console.log(this.datoSend);
    this.datoSend.dat = dato;  
    this.datoSend.unidat = unidaddato;
    this.datoSend.uniconv = unidadconv;
    this.rest.Convertir(this.datoSend).subscribe((result) => {
     // this.productData = result;
      this.datoReceived = result;
      console.log(result);
      //this.router.navigate(['/product-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
    
  }
  limpiar (){
    this.datoSend.unidat = "";
    this.datoSend.uniconv = "";
    this.datoSend.dat = "";
    this.datoReceived.uni_dat = "";
    this.datoReceived.uni_conv = "";
    this.datoReceived.dat = "";
    this.datoReceived.dat_conv = "";
    
  }

}
